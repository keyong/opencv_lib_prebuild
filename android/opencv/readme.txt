this is a special build for opencv 3.4.2 for android with openmp and neon flag to gain good performance.

build steps
1. change CMakefiles.txt, set WITH_OPENMP ON
2. cmake -DCMAKE_TOOLCHAIN_FILE=../platforms/android/android.toolchain.cmake --config release -DANDROID_ABI="armeabi-v7a with NEON" ..
